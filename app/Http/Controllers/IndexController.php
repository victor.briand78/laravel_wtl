<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $sales = Sale::all();

        return inertia('Index/Index', compact('sales'));
    }
}
