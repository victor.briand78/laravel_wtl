# Laravel_wtl

## In progress

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
This project requires the following steps to get started:

1.  Clone the project using the following command: `git clone git@gitlab.com:victor.briand78/laravel_wtl.git`
2.  Run the command `docker-compose up` in your project's terminal to mount and run the database
3.  Run the command `composer install` to install PHP dependencies
4.  Run the command `npm install` to install project dependencies
5.  Access the URL `localhost:8080` to use "adminer" and create a new database with the name "laravel-wtl" and "utf8mb4_general_ci" rules
6.  Run the command `php artisan migrate:refresh --seed` to seed the database
7.  Run the command `npm run dev`, and in a new terminal, run `php artisan serve` to start local servers
8.  If necessary, generate an app key using the command `php artisan key:generate`
