<?php

namespace Database\Seeders;

use App\Models\Sale;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'year' => '2010',
                'sales' => 1000,
            ],
            [
                'year' => '2011',
                'sales' => 10000,
            ],
            [
                'year' => '2012',
                'sales' => 100000,
            ],
            [
                'year' => '2013',
                'sales' => 1000000,
            ],
            [
                'year' => '2014',
                'sales' => 10000000,
            ],
            [
                'year' => '2015',
                'sales' => 100000000,
            ],
            [
                'year' => '2016',
                'sales' => 1000000000,
            ],
            [
                'year' => '2017',
                'sales' => 10000000000,
            ],
            [
                'year' => '2018',
                'sales' => 100000000000,
            ],
            [
                'year' => '2019',
                'sales' => 1000000000000,
            ],
            [
                'year' => '2020',
                'sales' => 10000000000000,
            ],
        ];
        Sale::insert($data);
    }
}